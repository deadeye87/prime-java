package com.example.prime;

import java.util.*;

class Prime {
    static void primeSum(int[] numList, int n)
    {

        int max_val = Arrays.stream(numList).max().getAsInt();

        Vector<Boolean> prime = new Vector<>(max_val + 1);
        for(int i = 0; i < max_val + 1; i++)
            prime.add(i,Boolean.TRUE);


        prime.add(0,Boolean.FALSE);
        prime.add(1,Boolean.FALSE);
        for (int p = 2; p * p <= max_val; p++)
        {

            if (prime.get(p))
            {

                for (int i = p * 2; i <= max_val; i += p)
                    prime.add(i,Boolean.FALSE);
            }
        }

        // Sum all primes in arr[]
        int sum = 0;
        for (int i = 0; i < n; i++)
            if (prime.get(numList[i]))
                sum += numList[i];

        return sum;
    }

    public static void main(String[] args)
    {
        int[] numList = { 1, 2, 3, 4, 5, 6, 7 };
        int n = numList.length;
        System.out.print(primeSum(numList, n));
    }
}
